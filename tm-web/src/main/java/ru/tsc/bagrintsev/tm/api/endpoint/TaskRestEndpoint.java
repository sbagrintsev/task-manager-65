package ru.tsc.bagrintsev.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.model.Task;

public interface TaskRestEndpoint {

    @PostMapping(produces = "application/json")
    void save(@RequestBody Task task);

    @DeleteMapping(value = "/{id}" , produces = "application/json")
    void delete(@PathVariable("id") String id);

    @GetMapping(value = "/{id}" , produces = "application/json")
    Task get(@PathVariable("id") String id);

}

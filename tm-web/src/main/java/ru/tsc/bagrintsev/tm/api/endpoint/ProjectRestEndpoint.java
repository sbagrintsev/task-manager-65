package ru.tsc.bagrintsev.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.model.Project;

public interface ProjectRestEndpoint {

    @PostMapping(produces = "application/json")
    void save(@RequestBody Project project);

    @DeleteMapping(value = "/{id}" , produces = "application/json")
    void delete(@PathVariable("id") String id);

    @GetMapping(value = "/{id}" , produces = "application/json")
    Project get(@PathVariable("id") String id);

}

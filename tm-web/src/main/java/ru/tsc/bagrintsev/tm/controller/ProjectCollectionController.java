package ru.tsc.bagrintsev.tm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.bagrintsev.tm.service.ProjectService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/projects")
public class ProjectCollectionController {

    private final ProjectService projectService;

    @GetMapping
    public ModelAndView get() {
        return new ModelAndView("project-list", "projects", projectService.findAll());
    }

}

package ru.tsc.bagrintsev.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService {

    @NotNull
    private final String APPLICATION_VERSION = "buildNumber";

    @NotNull
    private final String AUTHOR_EMAIL = "email";

    @NotNull
    private final String AUTHOR_NAME = "developer";

    @NotNull
    @Value("#{environment['passwordHash.iterations']}")
    private Integer passwordHashIterations;

    @NotNull
    @Value("#{environment['passwordHash.keyLength']}")
    private Integer passwordHashKeyLength;

    @NotNull
    @Value("#{environment['server.host']}")
    private String serverHost;

    @NotNull
    @Value("#{environment['server.port']}")
    private Integer serverPort;

    @NotNull
    @Value("#{environment['session.key']}")
    private String sessionKey;

    @NotNull
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @NotNull
    @Value("#{environment['database.username']}")
    private String databaseUserName;

    @NotNull
    @Value("#{environment['database.password']}")
    private String databaseUserPassword;

    @NotNull
    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @NotNull
    @Value("#{environment['database.sql.dialect']}")
    private String databaseSqlDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl.auto']}")
    private String databaseHbm2DdlAuto;

    @NotNull
    @Value("#{environment['database.pool.min']}")
    private String databasePoolMinSize;

    @NotNull
    @Value("#{environment['database.pool.max']}")
    private String databasePoolMaxSize;

    @NotNull
    @Value("#{environment['database.pool.timeout']}")
    private String databasePoolTimeout;

    @NotNull
    @Value("#{environment['database.show.sql']}")
    private String databaseShowSql;

    @NotNull
    @Value("#{environment['database.format.sql']}")
    private String databaseFormatSql;

    @NotNull
    @Value("#{environment['database.use.sql.comments']}")
    private String databaseUseSqlComments;

    @NotNull
    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String databaseUseSecondLevelCache;

    @NotNull
    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String databaseCacheProvConfigFileResPath;

    @NotNull
    @Value("#{environment['database.cache.region.factory_class']}")
    private String databaseCacheRegionFactoryClass;

    @NotNull
    @Value("#{environment['database.cache.use_query_cache']}")
    private String databaseCacheUseQueryCache;

    @NotNull
    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String databaseCacheUseMinimalPuts;

    @NotNull
    @Value("#{environment['database.cache.hazelcast.use_lite_member']}")
    private String databaseCacheHazelcastUseLiteMember;

    @NotNull
    @Value("#{environment['database.cache.region_prefix']}")
    private String databaseCacheRegionPrefix;

}

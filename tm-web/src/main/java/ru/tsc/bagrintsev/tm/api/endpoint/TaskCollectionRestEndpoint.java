package ru.tsc.bagrintsev.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;

public interface TaskCollectionRestEndpoint {

    @PostMapping(produces = "application/json")
    void save(@RequestBody List<Task> tasks);

    @DeleteMapping(produces = "application/json")
    void delete(@RequestBody List<String> taskIds);

    @GetMapping(produces = "application/json")
    List<Task> get();

}

package ru.tsc.bagrintsev.tm.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.bagrintsev.tm.dto.FormDTO;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.service.ProjectService;

@Controller
@RequiredArgsConstructor
@RequestMapping("/project/")
public class ProjectController {

    private final ProjectService projectService;

    @GetMapping("create")
    public ModelAndView create() {
        final Project project = new Project();
        final FormDTO form = new FormDTO();
        form.setProject(project);
        form.setReferer("/projects");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("form", form);
        return modelAndView;
    }

    @GetMapping("delete/{id}")
    public String delete(@PathVariable("id") String id) {
        projectService.deleteById(id);
        return "redirect:/projects";
    }

    @PostMapping("edit/{id}")
    public String edit(@ModelAttribute("project") final FormDTO form) {
        projectService.save(form.getProject());
        return "redirect:" + form.getReferer();
    }

    @GetMapping("edit/{id}")
    public ModelAndView edit(
            @RequestHeader String referer,
            @PathVariable("id") String id
    ) {
        final Project project = projectService.findById(id);
        final FormDTO form = new FormDTO();
        form.setProject(project);
        form.setReferer(referer);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("form", form);
        return modelAndView;
    }

    @PostMapping("create")
    public String post(@ModelAttribute("project") final FormDTO form) {
        projectService.save(form.getProject());
        return "redirect:" + form.getReferer();
    }

}

package ru.tsc.bagrintsev.tm.api.sevice;

import jakarta.xml.bind.JAXBException;
import ru.tsc.bagrintsev.tm.exception.entity.DomainNotFoundException;

import java.io.IOException;

public interface IDomainService {

    void loadBackup() throws IOException, DomainNotFoundException, ClassNotFoundException;

    void loadBase64() throws IOException, ClassNotFoundException, DomainNotFoundException;

    void loadBinary() throws IOException, ClassNotFoundException, DomainNotFoundException;

    void loadJacksonJSON() throws IOException, DomainNotFoundException;

    void loadJacksonXML() throws IOException, DomainNotFoundException;

    void loadJacksonYAML() throws IOException, DomainNotFoundException;

    void loadJaxbJSON() throws JAXBException, DomainNotFoundException;

    void loadJaxbXML() throws JAXBException, DomainNotFoundException;

    void saveBackup() throws IOException;

    void saveBase64() throws IOException;

    void saveBinary() throws IOException;

    void saveJacksonJSON() throws IOException;

    void saveJacksonXML() throws IOException;

    void saveJacksonYAML() throws IOException;

    void saveJaxbJSON() throws JAXBException;

    void saveJaxbXML() throws JAXBException;

}

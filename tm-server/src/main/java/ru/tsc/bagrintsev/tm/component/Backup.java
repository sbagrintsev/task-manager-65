package ru.tsc.bagrintsev.tm.component;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.exception.entity.DomainNotFoundException;
import ru.tsc.bagrintsev.tm.service.DomainService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public final class Backup {

    @NotNull
    final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final DomainService domainService;

    private void load() throws DomainNotFoundException, IOException, ClassNotFoundException {
        if (Files.exists(Paths.get(DomainService.FILE_BACKUP))) {
            domainService.loadBackup();
        }
    }

    @SneakyThrows
    private void save() {
        domainService.saveBackup();
    }

    public void start() throws DomainNotFoundException, IOException, ClassNotFoundException {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 5, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

}

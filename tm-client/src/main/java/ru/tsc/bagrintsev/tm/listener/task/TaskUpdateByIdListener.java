package ru.tsc.bagrintsev.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskUpdateByIdRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

@Component
public class TaskUpdateByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskUpdateByIdListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.ID);
        @Nullable final String id = TerminalUtil.nextLine();
        showParameterInfo(EntityField.NAME);
        @Nullable final String name = TerminalUtil.nextLine();
        showParameterInfo(EntityField.DESCRIPTION);
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(getToken());
        request.setTaskId(id);
        request.setName(name);
        request.setDescription(description);
        taskEndpoint.updateTaskById(request);
    }

    @NotNull
    @Override
    public String name() {
        return "task-update";
    }

}

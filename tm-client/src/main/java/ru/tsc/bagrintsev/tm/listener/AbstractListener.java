package ru.tsc.bagrintsev.tm.listener;

import jakarta.xml.bind.JAXBException;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.bagrintsev.tm.api.model.ICommand;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.service.TokenService;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Locale;

@NoArgsConstructor
public abstract class AbstractListener implements ICommand {

    @NotNull
    @Autowired
    protected TokenService tokenService;

    @NotNull
    public abstract String description();

    @NotNull
    public abstract Role[] getRoles();

    @SneakyThrows
    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    @SneakyThrows
    protected void setToken(@Nullable final String token) {
        if (token == null || token.isEmpty()) return;
        tokenService.setToken(token);
    }

    public abstract void handle(@NotNull final ConsoleEvent consoleEvent) throws IOException, AbstractException, GeneralSecurityException, ClassNotFoundException, JAXBException;

    @NotNull
    public abstract String name();

    @NotNull
    public abstract String shortName();

    protected void showOperationInfo() {
        System.out.printf("[%s]%n", name().toUpperCase(Locale.ROOT).replace('-', ' '));
    }

    protected void showParameterInfo(@NotNull final EntityField parameter) {
        System.out.printf("Enter %s: %n", parameter.getDisplayName());
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull StringBuilder result = new StringBuilder();
        result.append("[");
        result.append(String.format("%-35s", name()));
        result.append(" | ");
        result.append(String.format("%-25s", shortName()));
        result.append("]");
        if (!description().isEmpty()) {
            while (result.length() < 75) {
                result.append(" ");
            }
            result.append(description());
        }
        return result.toString();
    }

}
